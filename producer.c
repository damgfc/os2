/*
$Author: o-moring $
$Log: producer.c,v $
Revision 1.4  2015/02/26 13:13:55  o-moring
final

Revision 1.3  2015/02/24 22:37:10  o-moring
2/24

Revision 1.1  2015/02/16 17:44:57  o-moring
Initial revision

$Date: 2015/02/26 13:13:55 $
*/

/**************/
/* o-moring.2 */
/**************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "shared.h"

int main(int argc, char **argv){
	printf("BEGIN PRODUCER\n--------------------\n");	
	char mainBuffer[256], message[256]; 

	int i, j, k = 0;
	FILE *fp, *logFP;
	fp = fopen("input.txt", "r");
	logFP = fopen("prod.log", "a+");
	fclose(logFP);
	
	if (fp == NULL || logFP == NULL){
		perror("Can't open file\n");
	}

	sharedStruct *shmP; // Shared Memory Pointer

	/** Get shared memory access **/
	int shmid = shmget ( SHM_KEY, sizeof(sharedStruct), 0777 );
	
	if (shmid == -1){
		perror("Error in producer shmget");
		exit(1);
	}	
	
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
	
	/* Read from the file and add to the buffers */
	for ( ; ; ) {
		if (!shmP->eofFlag){ /** EOF has been picked up. Don't read into the buffer **/
			logFP = fopen("prod.log", "a+");
			writeToLog(logFP, "Started");

			// cycle through buffers
			for (i = 0; i < shmP->bufferFlagSize; i++) {
				if (!shmP->bufferFullFlag[i]){
					if (fgets(shmP->buffer[i], sizeof(shmP->buffer[i]), fp) != NULL){
						shmP->bufferFullFlag[i] = 1;
						sprintf(message, "Write %d %s", i, shmP->buffer[i]);
						writeToLog(logFP, message);
					}
					else {
						shmP->eofFlag = 1;
					}
				}
			}						
			fclose(logFP);
		}
	}  
	printf("--------------------\nEND PRODUCER\n");	
	return 0;
}
