#ifndef SHARED_H
#define SHARED_H

//extern SHM_KEY;
#define SHM_KEY 4255

	/***************/
	/* idle = 0    */
	/* want_in = 1 */
	/* in_cs = 2    */
	/***************/
enum state { idle, want_in, in_cs };


typedef struct {
	int bufferFullFlag[5];
	char buffer[5][128];
	int state[18]; // array of 18 state - each consumer needs state
	int turn; 
	int numberOfConsumers;
	int stateSize;
	int bufferFlagSize;
	int eofFlag;
	int termFlags[10]; // EOF, DIE, Don't Die
} sharedStruct;


int main(int argc, char **argv);
void writeToLog(FILE *fp, char *string);
int detachAndKill(int shmid, void *shmaddr);
int signalHandler();
int fireAlarm();

#endif
