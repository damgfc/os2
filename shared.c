/*
$Author: o-moring $
$Log: shared.c,v $
Revision 1.2  2015/02/26 13:13:55  o-moring
final

Revision 1.1  2015/02/24 22:37:10  o-moring
Initial revision

$Date: 2015/02/26 13:13:55 $
*/
#include <time.h>
#include <stdio.h>
#include <sys/shm.h>
#include <errno.h>
#include "shared.h"


void writeToLog(FILE *fp, char *string){
	int size = sizeof(string) + 10; // Make enough room for the HH:MM:SS
	char output[size], timeOut[10];
	
	time_t currentTime = time(NULL);
	struct tm *time;

	time = localtime(&currentTime);
	strftime(timeOut, 10, "%H:%M:%S", time);  
	sprintf(output, "%s\t%s\n", timeOut, string);	
	
	fprintf(fp, output);
}

int detachAndKill(int shmid, void *shmaddr){
	int err = 0;
	
	if (shmdt(shmaddr) == -1)
		err = errno;
	if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !err)
		err = errno;
	if (!err)
		return 0;
	errno = err;
	return -1;	
}

int signalHandler(){
	return 1;
}

int fireAlarm(){
	return 1;
}

