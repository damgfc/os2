/*
$Author: o-moring $
$Log: consumer.c,v $
Revision 1.5  2015/02/26 13:13:55  o-moring
final

Revision 1.4  2015/02/25 14:39:05  o-moring
shared memory works

Revision 1.3  2015/02/24 22:37:10  o-moring
2/24

$Date: 2015/02/26 13:13:55 $
*/

/**************/
/* o-moring.2 */
/**************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "shared.h"

sharedStruct *shmP;
int count;
FILE *logFP;
char filename[8];

int main(int argc, char **argv){
	printf("BEGIN CONSUMER\n--------------------\n");

	/** Make sure consumer is being called with a argument **/
    if (argc != 2){
    	perror("Improper consumer execution\nExpected `consumer [n]'");
    	exit(1);
    } 
    
    count = atoi(argv[1]);
    sprintf(filename, "cons%d.log", count);
	logFP = fopen(filename, "a+"); //Create the file then close it
	fclose(logFP);
	/** Get shared memory access **/
	int shmid = shmget ( SHM_KEY, sizeof(sharedStruct), 0777 );
	
	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}	
	
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
 	solution4(count);
	printf("--------------------\nEND CONSUMER\n");
	return 0;
}

solution4 ( const int i ){
	int j; // Local to each process
	do {
		do {
			shmP->state[i] = want_in; // Raise my flag
			j = shmP->turn; // Set local variable
			while ( j != i )
				j = ( shmP->state[j] != idle ) ? shmP->turn : ( j + 1 ) % shmP->numberOfConsumers;
			// Declare intention to enter critical section
			shmP->state[i] = in_cs;
			// Check that no one else is in critical section
			for ( j = 0; j < shmP->numberOfConsumers; j++ )
				if ( ( j != i ) && ( shmP->state[j] == in_cs ) )
					break;
		} while (( j < shmP->numberOfConsumers) || ( shmP->turn != i && shmP->state[shmP->turn] != idle ));
		// Assign shmP->turn to self and enter critical section
		shmP->turn = i;
		printf("here\n");
		criticalSection();
		// Exit section
		j = (shmP->turn + 1) % shmP->numberOfConsumers;
		while (shmP->state[j] == idle)
			j = (j + 1) % shmP->numberOfConsumers;
		// Assign shmP->turn to next waiting process; change own flag to idle
		shmP->turn = j;
		shmP->state[i] = idle;
		remainderSection();
	} while ( 1 );
}

criticalSection(){
	int i;
	for (i = 0; i < shmP->bufferFlagSize; i++){
		if (shmP->bufferFullFlag[i]){ 		// look for full flag set on
			printf("Consumer %d: %s\n", count, shmP->buffer[i]); 
		}
		// write to log
		logFP = fopen(filename, "a+");
		writeToLog(logFP, shmP->buffer[i]);
//		fprintf(logFP, shmP->buffer[i]); // Print the buffer message
		fclose(logFP);
		shmP->bufferFullFlag[i] = 0; // turn off full flag
	}
	
}

remainderSection(){
	//sleeps for random time
}

// 	printf("Child: NumOfChild: %d\n", shmP->numberOfConsumers);
// 	printf("Child: State[2]: %d\n", shmP->state[2]);
// 	printf("Child: BuffFlags[1]: %d\n", shmP->bufferFullFlag[1]);
