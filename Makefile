CC = gcc
CFLAGS = 
OBJSM = main.o shared.o
OBJSP = producer.o shared.o 
OBJSC = consumer.o shared.o
PROGM = master 
PROGP = producer
PROGC = consumer 

.SUFFIXES: .c .o

all: $(PROGM) $(PROGP) $(PROGC)

$(PROGM): $(OBJSM)
	$(CC) -g -o $@ $(OBJSM)

$(PROGP): $(OBJSP)
	$(CC) -g -o $@ $(OBJSP)

$(PROGC): $(OBJSC)
	$(CC) -g -o $@ $(OBJSC)

.c.o:
	$(CC) -c -o $@ $<
	
clean:
	rm *.o 
	rm $(PROGM) $(PROGP) $(PROGC)
