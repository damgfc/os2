/*
$Author: o-moring $
$Log: main.c,v $
Revision 1.5  2015/02/26 13:13:55  o-moring
final

Revision 1.4  2015/02/25 14:39:05  o-moring
shared memory works

Revision 1.3  2015/02/24 22:37:10  o-moring
2/24

$Date: 2015/02/26 13:13:55 $
*/

/**************/
/* o-moring.2 */
/**************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "shared.h"
#include "main.h"

/*change*/

#define BUFF_SIZE 1024

sharedStruct *shmP;

int main(int argc, char **argv){
	int numberOfConsumers;
	int i, j, k = 0;
	char consumerArg[3];
	int shmid;
	int mode;
		
	/** Shared memory creation and attachment **/
	shmid = shmget(SHM_KEY, sizeof(sharedStruct), IPC_CREAT | 0777);
	
	if (shmid == -1){  /** shmget failed **/
		perror("Unsuccessful shmget");
		exit(1);
	}
	else { /** shmget succeeded!! **/
		shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		if (shmP == (void *)-1)
			return -1;
			
		/** Get the Array sizes and set values to 0 **/
		shmP->bufferFlagSize = sizeof(shmP->bufferFullFlag)/(sizeof(int));
		shmP->stateSize = sizeof(shmP->state)/(sizeof(int));
		
		for (j = 0; j < shmP->bufferFlagSize; j++){
				shmP->bufferFullFlag[j] = 0;
		}
		
		for (j = 0; j < shmP->stateSize; j++){
				shmP->state[j] = 0;
		}
		
		/** Checks to make sure the program is called correctly **/
		if (argc != 2){
			printf("Master must be called with a number of consumers\n");
			printf("master [number]\n");
		}
		else {
			shmP->numberOfConsumers = atoi(argv[1]);
		}
	}
	
	/** Check value of the number input **/
	if (shmP->numberOfConsumers < 1 || 
			shmP->numberOfConsumers > 18){
		shmP->numberOfConsumers = 10;	
	}
		
	/** Go through loop to create 1 producer and n consumers **/
	for (i = 0; i < (shmP->numberOfConsumers + 1); i++){
		sleep(3);
		switch( fork()) {
			case -1:
					printf("Unable to fork\n");
					return 1;
			case 0:
					if (i == 0){
						//Exec to producer
						execl("./producer", "producer", NULL);	
					}
					else {
						//Exec to Consumer
						sprintf(consumerArg,"%d", i);
						if (execl("./consumer", "consumer", consumerArg ,NULL) == -1) {
							perror("Error in exec consumer");
						}
						else {
							printf("success...I guess\n");
						}
					}
					break;			
			default:
					break;
		}
	}
	return 0;	
}
